# Class Request Macro #
This is Auto Class Apply Macro (url: http://sugang.inha.ac.kr)

## Warning ##
	Request are not changed yet. So, Auto applying packet are unlike real browser packet
	
	This does not get request CSS, JS, another html file.

## Usage ##
Install dependencies
>pip install -r modules.txt

Run Program
>python run.py hakbun password

## Options ##
	usage: run.py [-h] [-t TIME] [-r REPEAT] UserId UserPw

	positional arguments:
	  UserId                User ID
	  UserPw                User PW

	optional arguments:
	  -h, --help            show this help message and exit
	  -t TIME, --time TIME  The option that when program start 0~24h:0~60m
	  -r REPEAT, --repeat REPEAT
	                        How will you repeat the program when It Fail. Default 1

	Examples:
		python run.py userId userPw
		python run.py userId userPw -t 8:30
		python run.py userId userPw -t 8:30 -r 10

## Technical Stack ##
- Python v2.7
- beautifulsoup4==4.3.2
- mechanize==0.2.5
- pyasn1==0.1.7
- rsa==3.1.4
- wsgiref==0.1.2


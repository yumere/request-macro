#!/usr/bin/env python
#coding: utf-8
__author__ = 'yumere'

import mechanize, urllib
import bs4
import rsa, base64
import time, sys, datetime, random
import argparse

from colorlib import *

class_list = list()

def hex2b64(h):
	b64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	i = 0
	ret = str()
	while True:
		if i+3 > len(h):
			break
		else:
			pass
		c = int(h[i: i+3], 16)
		ret += b64map[c >> 6] + b64map[c & 63]

		i += 3

	if i+1 == len(h):
		c = int(h[i, i+1], 16)
		ret += b64map[c << 2]

	elif i+2 == len(h):
		c = int(h[i:i+2], 16)
		ret += b64map[c >> 2] + b64map[(c & 3) << 4]

	while len(ret) & 3 > 0:
		ret += '='

	return ret

def Encrypt(s, modulus, exponent):

	(publicKey, b) = rsa.newkeys(512)
	publicKey.n = int(base64.b64decode(modulus).encode('hex'), 16)
	publicKey.e = int(base64.b64decode(exponent).encode('hex'), 16)

	splitSize = 53

	arTmp = list()
	i=0
	while True:
		if i < float(len(s)) / float(splitSize):
			arTmp.append(hex2b64(rsa.encrypt(s[i*splitSize: (i+1)*splitSize], publicKey).encode('hex')))
			pass
		else:
			break
		i += 1
	return "|".join(arTmp)

def sugang_login(id, pw):
	url = 'http://sugang.inha.ac.kr/sugang/Menu.aspx'
	cj = mechanize.CookieJar()

	browser = mechanize.Browser()
	browser.set_handle_robots(False)
	browser.set_cookiejar(cj)
	browser.set_handle_redirect(True)
	browser.set_handle_referer(True)

	browser.addheaders = [
		('User-Agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; WOW64; Trident/8.0; .NET4.0E; .NET4.0C; .NET CLR 3.5.30729; .NET CLR 2.0.50727; .NET CLR 3.0.30729)'),
		('Accept', 'text/html, application/xhtml+xml, */*'),
		('Accept-Encoding', 'gzip, deflate'),
		('Accept-Language', 'ko-KR')
	]

	try:
		# response = browser.open('http://sugang.inha.ac.kr/ITISWebCommon/xml/PublicKey.xml')
		# soup = bs4.BeautifulSoup(response.read())
		# modulus = soup('modulus')[0].string.encode('utf-8')
		# exponent = soup('exponent')[0].string.encode('utf-8')
		modulus = "zS//nDJXsGZRXLPHXmRz6/llTCD6gHyj2iM0gSXe7rSOkcre1oopqEn7Hmc6R1LR/Ng9yW50RNjr5wN33f2Dow=="
		exponent = "AQAB"
	except Exception, e:
		print red("[-] Get PublicKey Error")
		print e
		return False

	login_data = {
		'itisWebCommonPath': '/ITISWebCommon',
	    'itisExternalLinkSite': 'http://sugang.inha.ac.kr',
	    'reportRootPath': '/ITISWebCommon/report',
	    'htxtExportType': 'EXCEL',
	    'winClosed': 'open',
	    'errorMessage': '',
	    'informationMessage': '',
	    'confirmMessage': '',
	    'confirmMessageSecu': '',
	    'informationLeft': '',
	    '__EVENTTARGET': '',
	    '__EVENTARGUMENT': '',
	    '__VIEWSTATE': '/wEPDwUKLTI5NjQ1MjQxM2QYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFCWlidG5Mb2dpblgHvNbSA3TFTbL/2nHbM69WjUcD',
	    '__VIEWSTATEGENERATOR': '226E17C9',
	    'txtCode': '',
	    'txtPassword': '',
	    'ibtnLogin.x': 122,
	    'ibtnLogin.y': 19,
	    'hhdencId': Encrypt(id, modulus, exponent),
	    'hhdencPw': Encrypt(pw, modulus, exponent),
	    'hidLang': 'KOR'
	}

	try:
		browser.open(url, data=urllib.urlencode(login_data))
	except Exception, e:
		return False

	return browser

def getList(browser):
	url = "http://sugang.inha.ac.kr/sugang/SU_53001/Sugang_Save.ASPX"
	response = browser.open(url)

	if "GiveDateChkMsg" in response.geturl():
		return False

	data = response.read()
	soup = bs4.BeautifulSoup(data)
	try:
		trs = soup('table', {'id': 'dgList2'})[0]
	except Exception, e:
		print e
		return False

	trs = trs('tr')[1:]

	for tr in trs:
		tds = tr('td')
		haksu = tds[0].string
		name = tds[1].string
		left = tds[7].string

		class_list.append(haksu)

	return True

def applyClass(haksu, browser):
	print green("Apply: %s" % haksu)
	url = "http://sugang.inha.ac.kr/sugang/SU_53001/Sugang_Save.ASPX"

	data = {
		'itisWebCommonPath': '/ITISWebCommon',
	    'itisExternalLinkSite': 'http://sugang.inha.ac.kr',
	    'reportRootPath': '/ITISWebCommon/report',
	    '__VIEWSTATE': '/wEPDwUKMTA0MzA1NDE5Mg9kFgICAQ9kFhwCAQ8PFgQeCENzc0NsYXNzBQd4Zy1saXN0HgRfIVNCAgJkFipmD2QWAmYPDxYEHwAFCXRkRGVmYXVsdB8BAgJkZAIBD2QWEGYPDxYKHwAFCXRkRGVmYXVsdB4JQmFja0NvbG9yCqQBHgRUZXh0ZR4PSG9yaXpvbnRhbEFsaWduCyopU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5Ib3Jpem9udGFsQWxpZ24CHwECioAEZGQCAQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUD7IWAHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUD7JuUHwQLKwQCHwECioAEZGQCAw8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUD7ZmUHwQLKwQCHwECioAEZGQCBA8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUD7IiYHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUD66qpHwQLKwQCHwECioAEZGQCBg8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUD6riIHwQLKwQCHwECioAEZGQCBw8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUD7YagHwQLKwQCHwECioAEZGQCAg9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQExHwQLKwQCHwECioAEZGQCAQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAICDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgMPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBA8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIFDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBw8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDD2QWEGYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwMFATIfBAsrBAIfAQKKgARkZAIBDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgIPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAw8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIEDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgUPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIHDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPZBYQZg8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUBMx8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQE0HwQLKwQCHwECioAEZGQCAQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAICDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgMPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBA8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIFDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBw8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGD2QWEGYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwMFATUfBAsrBAIfAQKKgARkZAIBDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgIPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAw8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIEDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgUPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIHDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPZBYQZg8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUBNh8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCCA9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQE3HwQLKwQCHwECioAEZGQCAQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAICDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgMPDxYKHwAFCXRkRGVmYXVsdB8CCn8fAwUBMx8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKeh8DBQEyHwQLKwQCHwECioAEZGQCBg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIHDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgkPZBYQZg8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUBOB8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgp/HwMFATMfBAsrBAIfAQKKgARkZAIEDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgUPDxYKHwAFCXRkRGVmYXVsdB8CCnofAwUBMh8ECysEAh8BAoqABGRkAgYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBw8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIKD2QWEGYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwMFATkfBAsrBAIfAQKKgARkZAIBDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgIPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAw8PFgofAAUJdGREZWZhdWx0HwIKfx8DBQEzHwQLKwQCHwECioAEZGQCBA8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIFDw8WCh8ABQl0ZERlZmF1bHQfAgp6HwMFATIfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCCw9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQIxMB8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCn8fAwUBMR8ECysEAh8BAoqABGRkAgUPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIHDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgwPZBYQZg8PFgofAAUJdGREZWZhdWx0HwIKpAEfAwUCMTEfBAsrBAIfAQKKgARkZAIBDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgIPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAw8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIEDw8WCh8ABQl0ZERlZmF1bHQfAgp/HwMFATEfBAsrBAIfAQKKgARkZAIFDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBw8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIND2QWEGYPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwMFAjEyHwQLKwQCHwECioAEZGQCAQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAICDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgMPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBA8PFgofAAUJdGREZWZhdWx0HwIKfx8DBQExHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCDg9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQIxMx8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCDw9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQIxNB8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCEA9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQIxNR8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCEQ9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQIxNh8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCEg9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQIxNx8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCEw9kFhBmDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DBQIxOB8ECysEAh8BAoqABGRkAgEPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCAg8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIDDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgQPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCBQ8PFgofAAUJdGREZWZhdWx0HwIKpAEfA2UfBAsrBAIfAQKKgARkZAIGDw8WCh8ABQl0ZERlZmF1bHQfAgqkAR8DZR8ECysEAh8BAoqABGRkAgcPDxYKHwAFCXRkRGVmYXVsdB8CCqQBHwNlHwQLKwQCHwECioAEZGQCFA9kFhBmDw8WBB8ABQl0ZERlZmF1bHQfAQICZGQCAQ8PFgQfAAUJdGREZWZhdWx0HwECAmRkAgIPDxYEHwAFCXRkRGVmYXVsdB8BAgJkZAIDDw8WBB8ABQl0ZERlZmF1bHQfAQICZGQCBA8PFgQfAAUJdGREZWZhdWx0HwECAmRkAgUPDxYEHwAFCXRkRGVmYXVsdB8BAgJkZAIGDw8WBB8ABQl0ZERlZmF1bHQfAQICZGQCBw8PFgQfAAUJdGREZWZhdWx0HwECAmRkAgUPDxYCHwMFDOyImOqwleyLoOyyrWRkAgcPFgIeCWlubmVyaHRtbAWLAVvtlZnrsoggOiAxMjExMTY3MiZuYnNwOyZuYnNwOyZuYnNwOyDstZzshLHsnqwmbmJzcDsmbmJzcDsmbmJzcDsgNO2VmeuFhCZuYnNwOyZuYnNwOyZuYnNwOyDsu7Ttk6jthLDsoJXrs7Tqs7XtlZnqs7wo7Lu07ZOo7YSw7KCV67O06rO17ZWZKV1kAgkPPCsACwIADxYKHg1TZWxlY3RlZEluZGV4Av////8PHghEYXRhS2V5cxYAHgtfIUl0ZW1Db3VudAIDHglQYWdlQ291bnQCAR4VXyFEYXRhU291cmNlSXRlbUNvdW50AgNkARQrAAs8KwAEAQAWAh4KSGVhZGVyVGV4dAUM7ZWZ7IiY67KI7Zi4PCsABAEAFgIfCwUG67aE67CYPCsABAEAFgIfCwUJ6rO866qp66qFPCsABAEAFgIfCwUG7ZWZ7KCQPCsABAEAFgIfCwUM6rO866qp6rWs67aEPCsABAEAFgIfCwUM64u064u56rWQ7IiYPCsABAEAFgIfCwUM6rCc7ISk7ZWZ6rO8PCsABAEAFgIfCwUJ7J6s7IiY6rCVPCsABAEAFgIfCwUG7IKt7KCcPCsABAEAFgIfCwUG67OA6rK9PCsABAEAFgIfCwUBIxYCZg9kFgYCAQ9kFhZmDw8WAh8DBQdDU0U0MjAzZGQCAQ8PFgIfAwUDMDAxZGQCAg8PFgIfAwUi7Lu07ZOo7YSw7KCV67O06rO17ZWZIOyihe2VqeyEpOqzhGRkAgMPDxYCHwMFAzMuMGRkAgQPDxYCHwMFDOyghOqzte2VhOyImGRkAgUPDxYCHwMFCeuwleykgOyEnWRkAgYPDxYCHwMFGOy7tO2TqO2EsOygleuztOqzte2VmeqzvGRkAgcPDxYCHwMFBiZuYnNwO2RkAggPDxYCHwMFlAE8aW5wdXQgdHlwZT0nYnV0dG9uJyB2YWx1ZT0n7IKt7KCcJyBvbkNsaWNrPSdzdWdhbmdEZWxldGUoIkNTRTQyMDMiLCIwMDEiLCLsu7Ttk6jthLDsoJXrs7Tqs7XtlZkg7KKF7ZWp7ISk6rOEIiknIHN0eWxlPSdjdXJzb3I6aGFuZCcgY2xhc3M9J2J0LWluMic+ZGQCCQ8PFgIfAwWUATxpbnB1dCB0eXBlPSdidXR0b24nIHZhbHVlPSfrs4Dqsr0nIG9uQ2xpY2s9J2hha3N1Q2hhbmdlKCLsu7Ttk6jthLDsoJXrs7Tqs7XtlZkg7KKF7ZWp7ISk6rOEIiwiQ1NFNDIwMzAwMU4zLjAiKScgc3R5bGU9J2N1cnNvcjpoYW5kJyBjbGFzcz0nYnQtaW4yJz5kZAIKDw8WBB8CCn8fAQIIZBYCAgEPDxYCHwMFATFkZAICD2QWFmYPDxYCHwMFB0NTRTQzMDlkZAIBDw8WAh8DBQMwMDJkZAICDw8WAh8DBQ/tjIzsnbzsspjrpqzroaBkZAIDDw8WAh8DBQMzLjBkZAIEDw8WAh8DBQzsoITqs7XshKDtg51kZAIFDw8WAh8DBQnstZzrspTquLBkZAIGDw8WAh8DBRjsu7Ttk6jthLDsoJXrs7Tqs7XtlZnqs7xkZAIHDw8WAh8DBQUyMDEzMWRkAggPDxYCHwMFgQE8aW5wdXQgdHlwZT0nYnV0dG9uJyB2YWx1ZT0n7IKt7KCcJyBvbkNsaWNrPSdzdWdhbmdEZWxldGUoIkNTRTQzMDkiLCIwMDIiLCLtjIzsnbzsspjrpqzroaAiKScgc3R5bGU9J2N1cnNvcjpoYW5kJyBjbGFzcz0nYnQtaW4yJz5kZAIJDw8WAh8DBYEBPGlucHV0IHR5cGU9J2J1dHRvbicgdmFsdWU9J+uzgOqyvScgb25DbGljaz0naGFrc3VDaGFuZ2UoIu2MjOydvOyymOumrOuhoCIsIkNTRTQzMDkwMDJZMy4wIiknIHN0eWxlPSdjdXJzb3I6aGFuZCcgY2xhc3M9J2J0LWluMic+ZGQCCg8PFgQfAgp6HwECCGQWAgIBDw8WAh8DBQEyZGQCAw9kFhZmDw8WAh8DBQdHRUcyMDIzZGQCAQ8PFgIfAwUDMDAxZGQCAg8PFgIfAwUP6riw7LSI7J2867O47Ja0ZGQCAw8PFgIfAwUDMy4wZGQCBA8PFgIfAwUM6rWQ7JaR7ISg7YOdZGQCBQ8PFgIfAwUJ67CV7Jqp66eMZGQCBg8PFgIfAwUY7J2867O47Ja47Ja066y47ZmU7ZWZ6rO8ZGQCBw8PFgIfAwUGJm5ic3A7ZGQCCA8PFgIfAwWBATxpbnB1dCB0eXBlPSdidXR0b24nIHZhbHVlPSfsgq3soJwnIG9uQ2xpY2s9J3N1Z2FuZ0RlbGV0ZSgiR0VHMjAyMyIsIjAwMSIsIuq4sOy0iOydvOuzuOyWtCIpJyBzdHlsZT0nY3Vyc29yOmhhbmQnIGNsYXNzPSdidC1pbjInPmRkAgkPDxYCHwMFgQE8aW5wdXQgdHlwZT0nYnV0dG9uJyB2YWx1ZT0n67OA6rK9JyBvbkNsaWNrPSdoYWtzdUNoYW5nZSgi6riw7LSI7J2867O47Ja0IiwiR0VHMjAyMzAwMU4zLjAiKScgc3R5bGU9J2N1cnNvcjpoYW5kJyBjbGFzcz0nYnQtaW4yJz5kZAIKDw8WBB8CCn8fAQIIZBYCAgEPDxYCHwMFATNkZAILDxYCHwUFf+2YhOyerCAz6rO866qpJm5ic3A7Jm5ic3A77LSdIDntlZnsoJAmbmJzcDsmbmJzcDvsnqzsiJjqsJUgM+2VmeygkCZuYnNwOyZuYnNwO09DVSAw7ZWZ7KCQJm5ic3A7Jm5ic3A7W+y1nOuMgDE5IH4g7LWc7IaMM+2VmeygkF1kAg0PDxYCHwMFE+yImOqwleyLoOyyrSDsnoXroKVkZAIPDw8WAh8DBRztlZnsiJjrsojtmLgg67aE67CYIDEw7J6Q66asZGQCEQ8PZBYEHglvbmtleWRvd24FC2J0bkNsaWNrKCk7HgVzdHlsZQURaW1lLW1vZGU6aW5hY3RpdmVkAhMPD2QWAh4Hb25DbGljawUXcmV0dXJuIGNoZWNrSGFrc3UoJ04nKTtkAh0PDxYCHwMFGuqzvOuqqeuqheyXkCDsnZjtlZwg6rKA7IOJZGQCHw8PZBYEHwwFFHR4dEt3YW1va19FbnRlcktleSgpHw0FD2ltZS1tb2RlOmFjdGl2ZWQCIw8PFgIfAwUrKOyVniAy7J6Q66as7J207IOBIOyeheugpSwg7JiIIDog7Lu07ZOo7YSwKWRkAiUPDxYCHwMF5AEqIOqzteuMgC9JVOqzteuMgCDsiJjqsJXsg53rk6TsnYAg7IiY6rCV7Z2s66ed64u06riwIOumrOyKpO2KuOyXkOyEnCAn7ISg7IiY6rO866qpIOuvuOydtOyImCcg6rO866qp7J2EIOuLtOydgCDqsr3smrAsIOyVhOuemCDrs7gg7IiY6rCV7Iug7LKt7ZmU66m07JeQ7ISc64qUIO2RnOyLnOuQmOyngCDslYrsirXri4jri6QuKOqzte2VmeyduOymnSDsoJzsmbjtlZnqs7zripQg7ISg7YOdIOyCrO2VrSlkZAInDzwrAAsCAA8WCh8GAv////8PHwcWAB8IAgIfCQIBHwoCAmQBFCsACTwrAAQBABYCHwsFDO2VmeyImOuyiO2YuDwrAAQBABYCHwsFCeqzvOuqqeuqhTwrAAQBABYCHwsFBu2VmeygkDwrAAQBABYCHwsFDOqzvOuqqeq1rOu2hDwrAAQBABYCHwsFEuyLnOqwhOuwj+qwleydmOyLpDwrAAQBABYCHwsFDOuLtOuLueq1kOyImDwrAAQBABYCHwsFBuu5hOqzoDwrAAQBABYCHwsFBuyXrOyEnTwrAAQBABYCHwsFBuyLoOyyrRYCZg9kFgQCAQ9kFhJmDw8WAh8DBQtDU0U0MzAxLTAwMWRkAgEPDxYCHwMFD+yghOyekOyDgeqxsOuemGRkAgIPDxYCHwMFAzMuMGRkAgMPDxYCHwMFDOyghOqzteyEoO2DnWRkAgQPDxYCHwMFFO2ZlDEsMizrqqkzKO2VmC0yMjApZGQCBQ8PFgIfAwUJ7KGw6re87IudZGQCBg8PFgIfAwUP7JiB7Ja0LEVuZ2xpc2gsZGQCBw8PFgIfAwUCNDJkZAIIDw8WAh8DBW48aW5wdXQgdHlwZT0nYnV0dG9uJyB2YWx1ZT0n7Iug7LKtJyBvbkNsaWNrPSdhdHRlbmRMZWN0dXJlKCJDU0U0MzAxLTAwMSIpJyBzdHlsZT0nQ3Vyc29yOmhhbmQnIGNsYXNzPSdidC1pbjInPmRkAgIPZBYSZg8PFgIfAwULQ1NFNDMwMy0wMDFkZAIBDw8WAh8DBRXqsozsnoTtlITroZzqt7jrnpjrsI1kZAICDw8WAh8DBQMzLjBkZAIDDw8WAh8DBQzsoITqs7XshKDtg51kZAIEDw8WAh8DBRTtmZQ3LDgsOSwxMCjtlZgtMzIyKWRkAgUPDxYCHwMFCeydtOydgOyEnWRkAgYPDxYCHwMFBiZuYnNwO2RkAgcPDxYCHwMFAjMyZGQCCA8PFgIfAwVuPGlucHV0IHR5cGU9J2J1dHRvbicgdmFsdWU9J+yLoOyyrScgb25DbGljaz0nYXR0ZW5kTGVjdHVyZSgiQ1NFNDMwMy0wMDEiKScgc3R5bGU9J0N1cnNvcjpoYW5kJyBjbGFzcz0nYnQtaW4yJz5kZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WBgUKaWJ0bkluc2VydAUQaWJ0bkV4dHJhU2VhcmNoMQUPaWJ0bkxlY1NjaGVkdWxlBRBpYnRuSW5wdXRDb25maXJtBQppYnRuRGVsZXRlBRBpYnRuRXh0cmFTZWFyY2gyFCpHsrsrg/5961DVaS5F5m3Y3c4=',
	    '__EVENTVALIDATION': '/wEWGQK/wtC9CAL64r3HAgLKw+bOBgKKtarACALAiaqlBgLBg9SpDQLwo63oDQLy09SGDgKb5oqKAQLAieY2Apn7svwMAr7jjtQNAqbVnowKApv7zsULAvSwnYQPAtn477UBAuH6l50BApab7uEIAtqsjO8GAqvnn+UJAozv1PUFAtSujMoDAqG58YQCAoyt+o4CArqNoKwCX7yxObsjbN0fGm7SszH2DnAGTyA=',
	    'txtHaksu': "".join(str(haksu).split("-")),
	    'ibtnInsert.x': 0,
	    'ibtnInsert.y': 0
	}

	response = browser.open(url, data=urllib.urlencode(data))
	soup = bs4.BeautifulSoup(response.read())
	desc = soup('input', {'id': 'hhdHaksu'})[0]
	if 'value' in desc.attrs.keys():
		if "".join(haksu.split("-")) in str(desc['value']).split(","):
			return True

	return False

def timer(intputTime):
	year, month, day = str(datetime.datetime.now()).split(" ")[0].split("-")
	try:
		hour, minutes = intputTime.split(":")
	except Exception, e:
		print e
		print red("Does not fit format")
		sys.exit(0)

	startTime = datetime.datetime(int(year), int(month), int(day), int(hour), int(minutes))
	print green("Start At: "+str(startTime))

	while True:
		now = datetime.datetime.now()

		restHour, restMinutes, restSeconds = str(startTime - now).split(".")[0].split(":")

		# If there are more than one hours, Sleep program
		try:
			if int(restHour) > 1:
				time.sleep(1 * 60 * 60)
			elif int(restMinutes) > 3:
				time.sleep((int(restMinutes)-1) * 60)
		except Exception:
			pass

		print now

		if str(startTime) == str(now).split(".")[0]:
			print(green("Start!"))
			break
		else:
			continue

if __name__ == "__main__":

	sleepTime = 1

	usage = "[options arg1]"
	epilog = """Examples:
	python run.py userId userPw
	python run.py userId userPw -t 8:30
	python run.py userId userPw -t 8:30 -r 10"""

	parser = argparse.ArgumentParser(epilog=epilog, formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('UserId', nargs=1, help='User ID')
	parser.add_argument('UserPw', nargs=1, help='User PW')
	parser.add_argument('-t', '--time', dest='Time', help='The option that when program start 0~24h:0~60m')
	parser.add_argument('-r', '--repeat', type=int, dest='Repeat', help='How will you repeat the program when It Fail. Default 1')

	options = parser.parse_args()

	if options.Time: timer(options.Time)
	repeatCount = options.Repeat if options.Repeat else 1
	userId = options.UserId[0]
	userPw = options.UserPw[0]

	browser = sugang_login(userId, userPw)
	if browser:
		print green("[+] Login Success")
	else:
		print red("[-] Login Fail")
		sys.exit(-1)

	for i in range(repeatCount):
		if getList(browser):
			print green("Get List Success")
		else:
			print red(u"[-] 수강신청 기간이 아닙니다")
			if i == repeatCount-1:
				print red("[-] Program Fail. Please Restart")
				sys.exit(-1)

			time.sleep(1)

	while True:
		if len(class_list) == 0:
			break

		for i in class_list:
			if applyClass(i, browser):
				print green("%s Success" % i)
				del class_list[class_list.index(i)]
			else:
				print(yellow("%s Fail" % i))
				continue

		time.sleep(random.randrange(3))

	browser.close()
	print green("Complete")